﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



public class AnchoredCubesData
{
    public List<GameObject> Planes { get; private set; }
    public Transform AnchorTransform { get; private set; }
    public bool Clockwise { get; private set; }


    public AnchoredCubesData(List<GameObject> planes, Transform anchorTransform, bool clockwise)
    {
        this.Planes = planes;
        this.AnchorTransform = anchorTransform;
        this.Clockwise = clockwise;
    }
}


public class RubiksCubeManager : MonoBehaviour
{



    public delegate void RubiksCubeUpdateHandler(List<GameObject> objects);
    public static event RubiksCubeUpdateHandler OnRubiksCubeUpdate;



    public delegate void CubeSideRotateHandler(AnchoredCubesData Data);
    public static event CubeSideRotateHandler OnCubeSideRotate;



    /// <summary>
    /// States für die Rubiks Cube Statemachine, nötig damit man nicht zwei Seiten gleichzeitig rotieren kann
    /// </summary>
    public enum RubiksCubeStates
    {
        Idle,
        SideRotating
    }

    public enum RubiksCubeSideGroups
    {
        Top,
        Bottom,
        Left,
        Right,
        Front,
        Back,
        CenterH,
        CenterV,
        CenterD
    }

    public static RubiksCubeStates State;

    /// <summary>
    /// Wie lange soll es dauern um eine Seite zu rotieren?
    /// </summary>
    [SerializeField]
    float anchorRotationTime;



    struct AnchorObjectKV
    {
        public Transform anchor;
        public List<GameObject> objects;
    }


    /// <summary>
    /// Sub-Cubes des Rubiks Cubes
    /// </summary>
    public GameObject[] RubiksCubeArray = new GameObject[26];
    


    /// <summary>
    /// Mitte vom RubiksCube (alle Rotationen und Abstände werden von hier aus berechnet)
    /// </summary>
    [SerializeField]
    public Transform Origin;



    /// <summary>
    /// Anker um die der Rubikscube rotiert wird
    /// </summary>
    [SerializeField]
    Transform LeftAnchor;
    [SerializeField]
    Transform RightAnchor;
    [SerializeField]
    Transform TopAnchor;
    [SerializeField]
    Transform BottomAnchor;
    [SerializeField]
    Transform FrontAnchor;
    [SerializeField]
    Transform BackAnchor;
    [SerializeField]
    Transform CenterVAnchor;
    [SerializeField]
    Transform CenterHAnchor;
    [SerializeField]
    Transform CenterDAnchor;



    /// <summary>
    /// Einzelne Seiten des Rubiks Cube
    /// </summary>
    [SerializeField]
    List<GameObject> Up = new List<GameObject>();
    [SerializeField]
    List<GameObject> Down = new List<GameObject>();
    [SerializeField]
    List<GameObject> Left = new List<GameObject>();
    [SerializeField]
    List<GameObject> Right= new List<GameObject>();
    [SerializeField]
    List<GameObject> Front = new List<GameObject>();
    [SerializeField]
    List<GameObject> Back = new List<GameObject>();
    [SerializeField]
    List<GameObject> CenterH = new List<GameObject>();
    [SerializeField]
    List<GameObject> CenterV = new List<GameObject>();
    [SerializeField]
    List<GameObject> CenterD = new List<GameObject>();



    


    private static RubiksCubeManager _instance;
    public static Transform Koords;


    /// <summary>
    /// Initialisierung
    /// </summary>
    void Start()
    {
        int i = 0;
        foreach (Transform cube in Origin)
        {
            RubiksCubeArray[i] = cube.gameObject;
            i++;
        }
        _instance = GameObject.FindObjectOfType<RubiksCubeManager>();
        SetSides();
        UpdateRubiksCube();

        Koords = this.gameObject.transform;
    }



    /// <summary>
    /// Methode um die aktuellen Seiten des Rubiks-Cubes zu registrieren (wird anhand der Position der Sub-Cubes im Verhältnis zum Ursprung/Origin festgelegt)
    /// </summary>
    void SetSides()
    {

        Up = new List<GameObject>();
        Down = new List<GameObject>();
        Left = new List<GameObject>();
        Right = new List<GameObject>();
        Front = new List<GameObject>();
        Back = new List<GameObject>();
        CenterH = new List<GameObject>();
        CenterV = new List<GameObject>();
        CenterD = new List<GameObject>();

        foreach (GameObject o in RubiksCubeArray)
        {
            Transform cube = o.transform;

            //Koordinaten auf 1 Dezimalstelle Genauigkeit runden, damit Unity Ungenauigkeiten ausgeschlossen werden
            cube.localPosition = new Vector3(cube.localPosition.x * 10, cube.localPosition.y * 10, cube.localPosition.z * 10);
            cube.localPosition = new Vector3(Mathf.Round(cube.localPosition.x), Mathf.Round(cube.localPosition.y), Mathf.Round(cube.localPosition.z));
            cube.localPosition = new Vector3(cube.localPosition.x / 10, cube.localPosition.y / 10, cube.localPosition.z / 10);

           
            //Fallunterscheidung anhand der Koordinaten 
            if (cube.localPosition.x <= -1.2)
            {
                Right.Add(cube.gameObject);
            }
            else if (cube.localPosition.x == 0)
            {
                CenterV.Add(cube.gameObject);
            }
            else if (cube.localPosition.x >= 1.2)
            {
                Left.Add(cube.gameObject);
            }




            if (cube.localPosition.y <= -1.2)
            {
                Down.Add(cube.gameObject);
            }
            else if (cube.localPosition.y == 0)
            {
                CenterH.Add(cube.gameObject);
            }
            else if (cube.localPosition.y >= 1.2)
            {
                Up.Add(cube.gameObject);
            }




            if (cube.localPosition.z <= -1.2)
            {
                Back.Add(cube.gameObject);
            }
            else if (cube.localPosition.z == 0)
            {
                CenterD.Add(cube.gameObject);
            }
            else if (cube.localPosition.z >= 1.2)    
            {
                Front.Add(cube.gameObject);
            }




        }
    }
    


    /// <summary>
    /// Methode um eine Seite des Rubiks-Cubes zu rotieren
    /// </summary>
    /// <param name="BlockList">Seite die rotiert werden soll</param>
    /// <param name="targetRotation">Gradzahl auf die die Seite rotiert werden soll (im prinzip ist dieser Wert immer 90)</param>
    /// <param name="rotationSpace">Relativer in dem die Rotation stattfinden soll (sollte immer Space.Self sein)</param>
    /// <param name="toRotate">Um welchen Anker rotiert werden soll.</param>
    /// <param name="clockwise">Soll die Rotation im Uhrzeigersinn (true) oder gegen den Uhrzeigersinn (false) stattfinden</param>
    /// <returns></returns>
    IEnumerator RotateSide(List<GameObject> BlockList, float targetRotation, Space rotationSpace, Transform toRotate, bool clockwise)
    {
        State = RubiksCubeStates.SideRotating;
        Vector3 axis = toRotate.transform.localPosition;


        //Alle zu rotierenden Objekte als Child vom entsprechenden Anker setzten
        foreach (GameObject o in BlockList)
        {
            o.transform.parent = toRotate;
        }


        //Wenn die Rotation gegen den Uhrzeigersinn passieren soll, Rotation umkehren
        if (!clockwise)
            targetRotation *= -1;



        //Fallunterscheidung falls die Rotationsachse NICHT durch die Koordinaten des Ankerpunkts gegeben ist
        if (axis == Vector3.zero)
        {
            //Kurzschreibweise (mit Linq) um jedes Element aus der Liste zu überprüfen
            //Jedes Element überprüfen, da die Achse nur nicht definiert ist, wenn um eine der Mittigen achsen rotiert werden soll, hierbei hat aber jedes Element, das rotiert werden soll genau eine Koordinate = 0 -> Achse anhand der Elemente festlegen
            if (BlockList.TrueForAll(e => e.transform.localPosition.x == 0))
            {
                axis = Vector3.left;
            }
            else if (BlockList.TrueForAll(e => e.transform.localPosition.y == 0))
            {
                axis = Vector3.up;
            }
            else if (BlockList.TrueForAll(e => e.transform.localPosition.z == 0))
            {
                axis = Vector3.forward;
            }
        }

            //Rotationsschritt festlegen (abhängig von der Rotationszeit)
            float rotationAmount = targetRotation * (anchorRotationTime * Time.deltaTime);
            float currentRotated = 0f;

            //Anker rotieren (dank der Parent->Child Beziehung rotieren die Element richtig mit
            while (Mathf.Abs(currentRotated) < Mathf.Abs(targetRotation))
            {
                toRotate.Rotate(axis, rotationAmount, rotationSpace);
                currentRotated += rotationAmount;
                yield return null;
            }
        

        //Mögliche Ungenauigkeiten seitens Unity wegrunden
        toRotate.localRotation = new Quaternion(Mathf.Round(toRotate.localRotation.x), 
                                                Mathf.Round(toRotate.localRotation.y), 
                                                Mathf.Round(toRotate.localRotation.z), 
                                                Mathf.Round(toRotate.localRotation.w));






        AnchoredCubesData d = new AnchoredCubesData(FindPlanes(BlockList), toRotate, clockwise);

        //Übergebe Rotation des Ankers & Cubes die z.Z. am Anker hängen

        if (OnCubeSideRotate != null)
        {
            OnCubeSideRotate(d);
        }



        //Rotierte Elemente wieder als Childs des Ursprungs setzen
        foreach (GameObject o in BlockList)
        {

            o.transform.parent = Origin;
        }


        //Neue Ausrichtung des Rubiks-Cubes setzten
        SetSides();
        UpdateRubiksCube();
        State = RubiksCubeStates.Idle;
    }


    List<GameObject> FindPlanes(List<GameObject> cubes)
    {
        List<GameObject> output = new List<GameObject>();

        foreach (GameObject c in cubes)
        {
            foreach (Transform child in c.transform)
            {
                if (!output.Contains(child.gameObject))
                    output.Add(child.gameObject);
            }
        }

        return output;
    }


    /// <summary>
    /// Methode übergibt nach vorne zeigende Seite mithilfe eines Events an alle Listener
    /// </summary>
    void UpdateRubiksCube ()
    {
        List<GameObject> output = new List<GameObject>();
        foreach (GameObject o in Front)
        {
            foreach (Transform child in o.transform)
            {
                if (child.position.z >= -0.1)
                    output.Add(child.gameObject);
            }
        }


        OnRubiksCubeUpdate(output);

    }







    /// <summary>
    /// Funktionen um die Seiten des Rubiks Cubes zu rotieren
    /// </summary>
    /// <param name="clockwise">Soll die Rotation im Uhrzeigersinn oder gegen den Uhrzeigersinn passieren?</param>
    #region rotationFunctions
    public void RotateLeft(bool clockwise)
    {
        if (State == RubiksCubeStates.Idle)
            StartCoroutine(RotateSide(Left, 90f, Space.Self, LeftAnchor, clockwise));
    }
    public void RotateRight (bool clockwise)
    {
        if (State == RubiksCubeStates.Idle)
            StartCoroutine(RotateSide(Right, 90f, Space.Self, LeftAnchor, clockwise));
    }
    public void RotateFront(bool clockwise)
    {
        if (State == RubiksCubeStates.Idle)
            StartCoroutine(RotateSide(Front, 90f, Space.Self, RightAnchor, clockwise));
    }
    public void RotateBack(bool clockwise)
    {
        if (State == RubiksCubeStates.Idle)
            StartCoroutine(RotateSide(Back, 90f, Space.Self, BackAnchor, clockwise));
    }
    public void RotateCenterVertical(bool clockwise)
    {
        if (State == RubiksCubeStates.Idle)
            StartCoroutine(RotateSide(CenterV, 90f, Space.Self, CenterVAnchor, clockwise));
    }
    public void RotateCenterHorizontal(bool clockwise)
    {
        if (State == RubiksCubeStates.Idle)
            StartCoroutine(RotateSide(CenterH, 90f, Space.Self, CenterHAnchor, clockwise));
    }
    public void RotateCenterDepth(bool clockwise)
    {
        if (State == RubiksCubeStates.Idle)
            StartCoroutine(RotateSide(CenterD, 90f, Space.Self, CenterDAnchor, clockwise));
    }
    public void RotateTop(bool clockwise)
    {
        if (State == RubiksCubeStates.Idle)
            StartCoroutine(RotateSide(Up, 90f, Space.Self, TopAnchor, clockwise));
    }
    public void RotateBottom(bool clockwise)
    {
        if (State == RubiksCubeStates.Idle)
            StartCoroutine(RotateSide(Down, 90f, Space.Self, BottomAnchor, clockwise));
    }
    public static void RotateCubeSide(RubiksCubeSideGroups group, bool clockwise)
    {
        switch (group)
        {
            case RubiksCubeSideGroups.Top:
                _instance.RotateTop(clockwise);
                break;
            case RubiksCubeSideGroups.Bottom:
                _instance.RotateBottom(clockwise);
                break;
            case RubiksCubeSideGroups.Left:
                _instance.RotateLeft(clockwise);
                break;
            case RubiksCubeSideGroups.Right:
                _instance.RotateRight(clockwise);
                break;
            case RubiksCubeSideGroups.Front:
                _instance.RotateFront(clockwise);
                break;
            case RubiksCubeSideGroups.Back:
                _instance.RotateBack(clockwise);
                break;
            case RubiksCubeSideGroups.CenterV:
                _instance.RotateCenterVertical(clockwise);
                break;
            case RubiksCubeSideGroups.CenterH:
                _instance.RotateCenterHorizontal(clockwise);
                break;
            case RubiksCubeSideGroups.CenterD:
                _instance.RotateCenterDepth(clockwise);
                break;
            default:
                throw new System.Exception("Not a valid Rubiks cube Group.");
        }

    }




    public void RandomizeCube(int steps)
    {
        if (State == RubiksCubeStates.Idle)
            StartCoroutine(randomizeCube(steps));
    }

    IEnumerator randomizeCube(int steps)
    {
        AnchorObjectKV randomAnchoredObjects;
        for (int i = 0; i < steps; i++)
        {
            randomAnchoredObjects = GetRandomSide();
            yield return StartCoroutine(RotateSide(randomAnchoredObjects.objects, 90, Space.Self, randomAnchoredObjects.anchor, true));
        }
    }



    AnchorObjectKV GetRandomSide()
    {
        AnchorObjectKV output = new AnchorObjectKV();
        int coin = Random.Range(0, 9);

        switch (coin)
        {
            case 0:
                output.anchor = TopAnchor;
                output.objects = Up;
                break;
            case 1:
                output.anchor = BottomAnchor;
                output.objects = Down;
                break;
            case 2:
                output.anchor = LeftAnchor;
                output.objects = Left;
                break;
            case 3:
                output.anchor = RightAnchor;
                output.objects = Right;
                break;
            case 4:
                output.anchor = FrontAnchor;
                output.objects = Front;
                break;
            case 5:
                output.anchor = BackAnchor;
                output.objects = Back;
                break;
            case 6:
                output.anchor = CenterDAnchor;
                output.objects = CenterD;
                break;
            case 7:
                output.anchor = CenterHAnchor;
                output.objects = CenterH;
                break;
            case 8:
                output.anchor = CenterVAnchor;
                output.objects = CenterV;
                break;
        }

        return output;
    }

    #endregion

}
