﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;



//Laufrichtungen des Avatars
public enum Directions
{
    Up = (1 << 0),
    Right = (1 << 1),
    Down = (1 << 2),
    Left = (1 << 3)
}








/// <summary>
/// Klasse, die die Steuerung und Interaktionen des Avatars kontrolliert
/// </summary>
public class AvatarController : MonoBehaviour {



    public static event PointDataHandler OnChangeWorldPoint;


    /// <summary>
    /// Die Position des Avatars wird als Index der Gitternetzpunkte gespeichert (0 = links oben, 8 = rechts unten)
    /// </summary>
    int CurrentPosition;
    public int StartPosition;


    /// <summary>
    /// Event wird jedes mal gefired, wenn eine Taste gedrückt wird (sollte noch Taste übergeben)
    /// </summary>
    event KeyPressHandler WonderfulEventOfEpicness;


    void Awake()
    {
        //Bewegungsmethode an Tastendruck-Event Subscriben
        WonderfulEventOfEpicness += this.MoveInput;
        WonderfulEventOfEpicness += this.InteractInput;
        
    }


    /// <summary>
    /// Initialisierung
    /// </summary>
    void Start()
    {
        this.transform.localPosition = GridController.instance.GridPoints[StartPosition].Position;
        CurrentPosition = StartPosition;




    }



    void Update()
    {
        //MEGA DRECKIG (immer wenn eine Taste gedrückt wird Tastendruckevent firen)
        //Eigener Controller?

        if (Input.anyKeyDown)
        {
            if (Input.GetKeyDown(KeyCode.E))
                WonderfulEventOfEpicness(KeyCode.E);

            if (Input.GetKeyDown(KeyCode.W))
                WonderfulEventOfEpicness(KeyCode.W);

            if (Input.GetKeyDown(KeyCode.A))
                WonderfulEventOfEpicness(KeyCode.A);

            if (Input.GetKeyDown(KeyCode.S))
                WonderfulEventOfEpicness(KeyCode.S);

            if (Input.GetKeyDown(KeyCode.D))
                WonderfulEventOfEpicness(KeyCode.D);


        }
    }









    /// <summary>
    /// Fallunterscheidung für die Bewegung (u.U. noch schöner machen)
    /// </summary>
    void MoveInput(KeyCode Key)
    {

        switch (Key)
        {
            case KeyCode.W:
                Move(Directions.Up);
                break;
            case KeyCode.A:
                Move(Directions.Left);
                break;
            case KeyCode.S:
                Move(Directions.Down);
                break;
            case KeyCode.D:
                Move(Directions.Right);
                break;
            default:
                break;
        }
    }

    void InteractInput(KeyCode Key)
    {
        switch (Key)
        {
            case KeyCode.E:
                GridController.instance.GridPoints[CurrentPosition].ExecutePointFunction();
                break;
            default:
                break;
        }
    }









    /// <summary>
    /// Bewegungsmethode (Wenn das aktuelle Feld in die gewünschte Richtung verlassbar ist, in das gewünschte Feld bewegen)
    /// </summary>
    /// <param name="dir">Richtung, in die man sich bewegen will</param>
    void Move(Directions dir)
    {
        //Fallunterscheidung anhand der Bitmask des aktuellen Felds (funktioniert nur, wenn sich der RubiksCube nicht rotiert)
        if (RubiksCubeManager.State == RubiksCubeManager.RubiksCubeStates.Idle)
        {
            // checken, ob ein Ausgang in Richtung vorhanden ist und ob ein eingang im entsprechenden angrenzenden Feld vorhanden ist
            switch (dir)
            {
                case Directions.Up:
                    if ((GridController.instance.GridPoints[CurrentPosition].Data.Exit & Exits.Top) == Exits.Top && (GridController.instance.GridPoints[CurrentPosition - 3].Data.Entrance & Exits.Bottom) == Exits.Bottom)
                        CurrentPosition = GridController.instance.GridPoints[CurrentPosition].ID - 3;
                    break;
                case Directions.Down:
                    if ((GridController.instance.GridPoints[CurrentPosition].Data.Exit & Exits.Bottom) == Exits.Bottom && (GridController.instance.GridPoints[CurrentPosition + 3].Data.Entrance & Exits.Top) == Exits.Top)
                        CurrentPosition = GridController.instance.GridPoints[CurrentPosition].ID + 3;
                    break;
                case Directions.Left:
                    if ((GridController.instance.GridPoints[CurrentPosition].Data.Exit & Exits.Left) == Exits.Left && (GridController.instance.GridPoints[CurrentPosition - 1].Data.Entrance & Exits.Right) == Exits.Right)
                        CurrentPosition = GridController.instance.GridPoints[CurrentPosition].ID - 1;

                    break;
                case Directions.Right:
                    if ((GridController.instance.GridPoints[CurrentPosition].Data.Exit & Exits.Right) == Exits.Right && (GridController.instance.GridPoints[CurrentPosition + 1].Data.Entrance & Exits.Left) == Exits.Left)
                        CurrentPosition = GridController.instance.GridPoints[CurrentPosition].ID + 1;
                    
                    break;
                default: break;

            }
            

            this.transform.localPosition = GridController.instance.GridPoints[CurrentPosition].Position;

            if (OnChangeWorldPoint != null)
                OnChangeWorldPoint(GridController.instance.GridPoints[CurrentPosition].Data);

        }

    }

}
