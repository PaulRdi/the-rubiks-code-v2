﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class RubiksCubePlaneData : MonoBehaviour {


    [SerializeField]
    public WorldPointData Data;

    Vector3 planeAxis;

    void Awake()
    {
        RubiksCubeManager.OnCubeSideRotate += RubiksCubeManager_OnCubeSideRotate;
    }

    void Start()
    {
        Data.Entrance = Data.Exit;

    }

    void Destroy()
    {
        RubiksCubeManager.OnCubeSideRotate -= RubiksCubeManager_OnCubeSideRotate;
    }

    //
    void RubiksCubeManager_OnCubeSideRotate(AnchoredCubesData d)
    {

        planeAxis = this.transform.up;

        if (d.Planes.Contains(this.gameObject) && planeAxis == d.AnchorTransform.localPosition.normalized)
        {
            //0001

            Debug.Log("Keks32");    
            if (d.Clockwise)
            {
                int val = (int)Data.Exit << 1;
                Data.Exit = (Exits)((val | (val >> 4)) & (int)Exits.All);
            }
            else
            {
                int val = (int)Data.Exit;
                Data.Exit = (Exits)(((val >> 1) | val << 3) & (int)Exits.All);
            }
            Data.Entrance = Data.Exit;

        }

    }


}
