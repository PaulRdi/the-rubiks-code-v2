﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;


/// <summary>
/// Controller für das Gitternetz in dem sich der Avatar bewegt
/// </summary>
public class GridController : MonoBehaviour
{


    public static GridController instance
    {
        get
        {
            if (_instance == null)
                    _instance = GameObject.FindObjectOfType<GridController>();
            return _instance;
        }
    }
    private static GridController _instance;


    /// <summary>
    /// Array der Gitternetzpunkte
    /// </summary>
    public WorldPoint[] GridPoints = new WorldPoint[9];



    /// <summary>
    /// Initialisierung (Array der Gitternetzpunkte populieren)
    /// </summary>
    void Awake()
    {
        int i = 0;
        foreach (WorldPoint p in GetComponentsInChildren<WorldPoint>())
        {
            GridPoints[i] = p;
            i++;
        }

   





        // An das Event im Rubiks-Cube-Manager subscriben, damit die nach vorne zeigende Seite übergeben wird.
        RubiksCubeManager.OnRubiksCubeUpdate += RubiksCubeManager_OnRubiksCubeUpdate;
    }


    void OnDestroy()
    {
        RubiksCubeManager.OnRubiksCubeUpdate -= RubiksCubeManager_OnRubiksCubeUpdate;
    }


    void RubiksCubeManager_OnRubiksCubeUpdate(List<GameObject> objects)
    {

        foreach (GameObject o in objects)
        {
            foreach (WorldPoint GridPoint in GridPoints)
            {
                if (Global.RoundToDecimal(GridPoint.Position.x, 1) == Global.RoundToDecimal(o.transform.position.x, 1) && Global.RoundToDecimal(GridPoint.Position.y, 1) == Global.RoundToDecimal(o.transform.position.y, 1))
                {
                    if (o.GetComponent<RubiksCubePlaneData>() != null)
                        GridPoint.Init(o.GetComponent<RubiksCubePlaneData>().Data);
                }
            }
        }
    }


}
